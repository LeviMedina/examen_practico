package prueba;

/**
 * @author Levito_Medina
 */
import java.util.Scanner;

public class Ejercicio1 {

    public static void main(String[] args) {
        int Suma = 0;

        Scanner sc = new Scanner(System.in);
        int n;

        System.out.println("Ingresa los numero de tu Matricula");
        System.out.println("Cuando termines indicalo con un 0");
        do {
            n = sc.nextInt();
            if (n == 0) {
                System.out.println("Indicaste" + " " + n + " " + "Proceso Finalizado");
            } else {
                if (n > 0) {
                    System.out.println("El numero " + n);
                    Suma = Suma + n;
                } else {
                    System.out.println("El numero " + n + " es negativo");
                }
            }
        } while (n != 0);
        System.out.println("La Suma de los Numeros Introducidos es " + Suma);

        //Indicar el Digito Verificador         
        if (Suma < 19) {
            System.out.println("El Digito Verificador es = 0 ");
        } else {
            if (Suma > 20 && Suma <= 29) {
                System.out.println("El Digito Verificador  es = 1");
            } else {
                System.out.println("El Digito Verificador  es = 2");
            }
        }
    }
}
